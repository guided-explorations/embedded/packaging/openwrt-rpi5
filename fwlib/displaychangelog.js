const package = require('./package.json')
const fs = require('node:fs');

console.log('Displaying the contents of all files starting with CHANGELOG (case insensitive) for ' + package.bin.fwfile + ' version: ' + package.version);


if (package.version.includes('alpha')) {
  console.log('Warning: Alpha version!')
}
  
fs.readdirSync('.').forEach(file => {
  if (file.match(/^changelog/i)) { 
    console.log(fs.readFileSync(file, 'utf8'));
  }
});