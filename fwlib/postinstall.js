const package = require('./package.json')
const fs = require('node:fs');
const path = require('path');
const os = require('os'); 
const tempdir = os.tmpdir();

packagebasepath = path.basename(path.resolve('.'));
relativepath = "../" + packagebasepath;

newdir = tempdir + "/" + packagebasepath

if (path.resolve(relativepath) == newdir){
  console.log('It appears you have already run postinstall and may already be running in ' + newdir + ', no action is being taken. Use `npm run` here to see other available commands')
} else {
  if (!fs.existsSync(newdir)){
    fs.mkdirSync(newdir);
  }
  console.log('Copying files to ' + newdir );
  fs.cpSync(relativepath, newdir, {recursive: true});
  console.log('Now change directories to ' + newdir)
  console.log('And then execute `npm run` to see available commands')
  console.log('You can keep your disk clean by deleting "'  + newdir + '" when done with the files.')
}
