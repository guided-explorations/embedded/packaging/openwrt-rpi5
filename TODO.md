
- Add GitLab Release (Already done in another Guided Exploration)
- [DONE] Sigstore signing
- Add Auto version numbering (Already done in another Guided Exploration)
- fully template package.json so no manual coding
- DONE Have npm install list the CHANGELOG after extraction (may be possible through scripts - but then requires global installation)
- Have npm install clean up all metafiles? (delete package.json, package-lock.json, bring deep content from node_modules to current directory and remove node_modules)
- Show an example (or have a switch) to encode the version number in the image file name (common in firmware)\
- npm package link to release links
- sigstore link to release links

## Hiding NPM Details for non Node Devs
1. create shell scripts for sh and powershell that:
  1. Does an `npm install @scope/package` (NON Global)
  2. Does an `npm --prefix node_modules/@scope/package` run postinstall
  3. Post install:
     1. Moves their current directory to node_modules/@scope/package
     2. Displays CHANGELOG
  4. MORE COMPLEX (especially if developer also does node dev): postinstall js acts like a self extractor shell by:
     - cleans files from current directory
     - moves extracted files to current directory
     - cleans up "node_modules" subdirectory
     - displays CHANGELOG