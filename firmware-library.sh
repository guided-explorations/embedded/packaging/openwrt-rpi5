
# Check if node is installed and if not, prompt if it is ok to install

if [[ -z "$(command -v npm)" ]] ; then 
  echo "Please install NPM using brew, yum, apt-get, chocolatey or whatever package manager you are used to"; 
fi

cd /tmp

#Install requested package
npm install "$1" 

#Show CHANGELOG
npm run --prefix node_modules/$1 postinstall