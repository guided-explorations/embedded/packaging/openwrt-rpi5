
See the [Why Manage a Developer Archive Library of Binary Images for Embedded Development Using NPM](https://gitlab.com/guided-explorations/embedded/packaging)

### Usage:

```
cd /tmp
npm config set registry https://gitlab.com/api/v4/packages/npm/
npm i @guided-explorations/openwrt-rpi5
npm --prefix node_modules/@guided-explorations/openwrt-rpi5 run postinstall
cd /tmp/openwrt-rpi5
node run displaychangelog
node run # see available commands
cd ..
#Clean up
rm -rf openwrt-rpi5
```

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2024-05-01

* **GitLab Version Released On**: 16.11


## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.
